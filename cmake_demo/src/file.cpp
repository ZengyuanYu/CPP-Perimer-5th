#include "file.hpp"

void file_op::write_file() {
    std::ofstream output;
     //创建文件
     output.open(this->nFileName);
     //写数据 output 类似于cout 
     output << "yuzengyuan" << " " << 100 << std::endl;
     //关闭文件 
     output.close();
}

void file_op::read_file() {
      std::ifstream input;
      //打开文件
      input.open(this->nFileName);
      //读数据 input 
      char name[80];
      int score; 
      input >> name >> score;
      std::cout << name << " " << score << std::endl;
      //关闭文件 
      input.close();
    }

