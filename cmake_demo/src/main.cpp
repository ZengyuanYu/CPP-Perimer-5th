#include "car.hpp"
#include "file.hpp"
#include <iostream>

int main(void) {
    std::cout << "hello world!" << std::endl;
    // 1.class eg.car
    car *myCar = new car("Dazhong");
    myCar->run();
    delete myCar;
    // 2.file operation
    // TODO(yuzengyuan): add write_file input [name data]
    file_op *myFile = new file_op("test.txt");
    myFile->write_file();
    myFile->read_file();
    delete myFile;
    system("pause");
    return 0;
}