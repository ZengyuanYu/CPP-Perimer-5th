#include <iostream>
#include <fstream>
#include <string>

class file_op
{
private:
    std::string nFileName;
public:
    file_op(std::string &&name) : nFileName(name) {
        std::cout << "now file name is:  "  << nFileName << std::endl;
    }
    void write_file();
    void read_file();
    ~file_op() {}
};