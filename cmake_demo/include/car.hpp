// #pragma once
#include <iostream>
#include <string>

class car
{
private:
    std::string nName;
public:
    car(std::string &&name) : nName(name) {
        std::cout << "car_name:   " << nName << "have creat" << std::endl;
    }
    ~car() {
        std::cout << "car_name:   " << nName << "have destory" << std::endl;
    }

    void run(void);
};