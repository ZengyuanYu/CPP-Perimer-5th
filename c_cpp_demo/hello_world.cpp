#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

int main(void) {
    std::vector<int> v1 = {1, 2, 3, 4, 5, 6};
    std::sort(v1.begin(), v1.end(), std::greater<int>());
    for (auto val : v1) {
        std::cout << val << " ";
    }
    std::cout << std::endl;

    auto printStr = [](std::string str) {
      std::cout << str << std::endl;  
    };
    printStr("Hello world!");
    std::cout << "hello_world!" << std::endl;
    system("pause");
    return 0;
}