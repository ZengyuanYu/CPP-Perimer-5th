#### VScode for C++ windows10配置教程

1. 安装VScode，无脑下一步
2. 安装MingW64
   - 安装包需要进行用管理员模式，不然会安装失败
   - 添加系统环境变量，比如`C:/mingw/bin`
3. 创建工程，目录为：
```
-- root
   - .vscode
   - build
   - XX.cpp
   - ...
```
4. `.vscode`文件已经包含在Test目录中，可以直接copy使用，需要注意的是json文件中MingW64的目录要写对。