#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <memory>

bool Compare(const int& a, const int& b, bool c) {
  c = a > b ? true : false;
  return c;
} 

int main(void) {
  // 1.pointer
  int nArray[3] = {1, 2, 3};
  int* ptr = nArray;
  std::cout << ptr << std::endl;  // addr
  std::cout << *ptr << std::endl;  // value
  ptr++;
  std::cout << ptr << std::endl;  // addr
  std::cout << *ptr << std::endl;  // value
  // 1.1 shard_ptr
  std::shared_ptr<int> p = std::make_shared<int>(42);
  auto p2 = std::make_shared<int>(42);
  
  // 2.reference
  int value = 9;
  int* ptr_value = &value;
  int &value_reference = value;
  std::cout << ptr_value << std::endl;  // addr
  std::cout << *ptr_value <<std::endl;  // value
  std::cout << &value_reference << std::endl;  // addr
  std::cout << value_reference << std::endl;  // value
  // 2.1 function refernce
  int value1 = 13;
  int value2 = 12;
  bool c = false;
  c = Compare(value1, value2, c);
  std::cout << c << std::endl;
  return 0;
}
/* result
// 1
0x63fe2c
1
0x63fe30
2
// 2
0x63fe28
9
0x63fe28
9
// 2.1
1
*/