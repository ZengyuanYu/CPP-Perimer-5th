### Template

#### 1. function template
  **模板**，简单来说就是利用一个符号来泛指所有类型，这就是所谓的泛型（generic）。比如需要知道两个*对象*的最大值，但不确定*对象*的类型（int/double/bool/std::string）。这时就可以使用模板了。一般形式为：
``` C++
template <typename T> // 加上这行来表示这是一个模板
(inline) T const& max(T const &x, T const &y)
{
    return x < y ? y : x;
}
```
如果有特例，比如int类型时候需要返回最小值，那么需要重新定义一个，在文中可以看到具体实现*特例*

#### 2. class template
  与函数模板类似，本质上为了处理不同的数据类型，其类内部函数亦可作为模板函数。