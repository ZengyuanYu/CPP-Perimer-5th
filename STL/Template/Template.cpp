#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>

// function template
template <typename T>
inline T const& Max (T const &x, T const &y) {
  return (x > y ? x : y);
} 
// template specialization
template <>
inline int const& Max<int> (const int  &x, const int &y) {
  return (x > y ? y : x); // return min (x, y)
} 
template <class T>
class Stack {
  public:
  void push(T const&);
  void pop();
  T top() const;
  bool empty() const{
    return elems_.empty();
  }

  private:
  std::vector<T> elems_;
};

//  入栈
template <class T>
void Stack<T>::push(T const& elem) {
  elems_.push_back(elem);
}
//  出栈
template <class T>
void Stack<T>::pop() {
  if (elems_.empty()) {
    throw std::out_of_range("Stack<>::pop empty stack");
  }
  return elems_.pop_back();
}
// 返回栈顶元素
template <class T>
T Stack<T>::top() const {
  if (elems_.empty()) {
    throw std::out_of_range("Stack<>::pop empty stack");
  }
  return elems_.back();
}
/*
int main(void) {
  // function template unit test
  int x = 19;
  int y = 255;
  std::cout << Max(x, y) << std::endl;

  double u = 19.99;
  double i = 19.22;
  std::cout << Max(u, i) << std::endl;

  std::string h = "aoliao";
  std::string w = "kebike";
  std::cout << Max(h, w) << std::endl;
  // class template uint test
  Stack<int> intStack;
  intStack.push(7);
  auto elem = intStack.top();
  intStack.pop();
  std::cout << elem << std::endl;   
  // elem = intStack.top();

  Stack<std::string> stringStack;
  stringStack.push("hello");
  auto stringElem = stringStack.top();
  // stringStack.pop();
  std::cout << stringElem << std::endl; 
  // return 0;
}
*/