#include <iostream>
#include <string>
#include <map>
#include <algorithm>

int main(void) {
    std::map<int ,std::string> Tmap; 
    Tmap[0] = "name";
    Tmap[1] = "address";
    Tmap[2] = "number";
    std::map<int, std::string>::iterator iter;
    for (iter = Tmap.begin(); iter != Tmap.end(); iter++) {
      std::cout << iter->first << "--" << iter->second << std::endl;
    }
    iter = Tmap.find(0);
    if (iter != Tmap.end()) {
      std::cout << "The find key's value is: " << iter->second << std::endl;
    }
    return 0;
}