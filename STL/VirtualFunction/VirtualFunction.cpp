#include <iostream>

class Base {
  public:
  Base() {};
  virtual void foo() {
      std::cout << "class Base foo function!" << std::endl;
  }  
  virtual void PrueVirtualFunc() = 0;
};

class B : public Base {
    public:
    B() {};
    virtual void foo() {
        std::cout << "class B foo function!" << std::endl;
    }
    virtual void PrueVirtualFunc() override;
};
void B::PrueVirtualFunc() {
        std::cout << "B prue virtual function" << std::endl;
    } 

int main(void) {
//    Base *base = new Base();
//    base->foo(); // print class Base foo
//
    Base *b = new B();
    b->foo(); // print class B foo
    b->PrueVirtualFunc();

    B *bb = new B();
    bb->foo(); // print class B foo
    b->PrueVirtualFunc();

    delete b;
    delete bb;
    return 0;
}